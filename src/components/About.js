import React from 'react';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import PSI_VERSION from '../utils/version';

const styles = theme => ({
  about: {
    margin: 10,
    padding: 10,
  },
});


class About extends React.Component {
  render() {
    const { classes } = this.props;
    const info = `
      PSI是一款基于SaaS模式(Software as a Service软件即服务)的企业管理软件。
      PSI以商贸企业的核心业务：采购、销售、库存（进销存）为切入点，最终目标是行业化的ERP解决方案。
    `;

    return (
      <Paper className={classes.about} elevation={4}>
        <Typography variant="headline" component="h3">
          关于PSI
        </Typography>
        <br />
        <Typography component="p">
          {info}
        </Typography>
        <br />
        <Typography component="p">
          版本号：{PSI_VERSION}
        </Typography>
      </Paper>
    );
  }
}

export default withStyles(styles)(About);

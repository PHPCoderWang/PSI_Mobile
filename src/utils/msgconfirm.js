import React from 'react';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

class MsgCofirm extends React.Component {

  render() {
    const { info, open, onOK, onCancel, ...other } = this.props;

    return (
      <Dialog open={open} {...other}>
        <DialogContent>
          <DialogContentText>
            {info}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onCancel} color="primary" autoFocus>
            取消
          </Button>
          <Button onClick={onOK} color="primary">
            确定
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
};

MsgCofirm.propTypes = {
};

export default MsgCofirm;

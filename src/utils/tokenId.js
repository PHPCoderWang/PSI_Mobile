const tokenName = 'psiLoginToken';

export function setPSITokenId(tokenId) {
    sessionStorage.setItem(tokenName, tokenId);
}

export function getPSITokenId() {
    return sessionStorage.getItem(tokenName);
}

export function removePSITokenId() {
    sessionStorage.removeItem(tokenName);

    localStorage.removeItem('PSI_password');
}
const styles = theme => ({
  r: {
    flexGrow: 1,
    height: '100vh'
  },
  h: {
    width: '100%'
  },
  c: {
    flexGrow: 1,
    overflow: 'auto',
    width: '100%'
  },
  f: {
    height: 50,
    width: '100%'
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
});

export default styles;

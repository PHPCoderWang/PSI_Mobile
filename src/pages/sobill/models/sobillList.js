import * as sobillService from '../services/sobillService';

export default {
  namespace: 'sobillList',
  state: {
    // 销售订单List
    list: [],

    // 总页数
    totalPage: 0,

    // 当前页码
    page: 1,

    entity: {
    },
  },

  reducers: {
    save(state, { payload: { data: list, totalPage, page } }) {
      return { ...state, list, totalPage, page };
    },

    saveEntity(state, { payload: { entity } }) {
      return { ...state, entity };
    },

  },

  effects: {
    *querySOBillList({ payload: { page = 1 } }, { call, put }) {
      const { data: result } = yield call(sobillService.querySOBillList, { page });

      yield put({
        type: 'save',
        payload: {
          data: result.dataList,
          totalPage: result.totalPage,
          page
        },
      });
    },

    *querySOBillInfo({ payload: { id } }, { call, put }) {
      const { data: entity } = yield call(sobillService.querySOBillInfo, { id });
      yield put({
        type: 'saveEntity',
        payload: {
          entity
        },
      });
    },

  },

  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname, query }) => {
        if (pathname === '/sobill') {
          dispatch({ type: 'querySOBillList', payload: query });
        }
      });
    },
  },
};

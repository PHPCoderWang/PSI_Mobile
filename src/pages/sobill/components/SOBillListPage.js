import React from 'react';
import { connect } from 'dva';
import router from 'umi/router';

import { getPSITokenId } from '../../../utils/tokenId';

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

import LeftArrowIcon from '@material-ui/icons/KeyboardArrowLeft'
import RightArrowIcon from '@material-ui/icons/KeyboardArrowRight'

import Grid from '@material-ui/core/Grid';
import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider } from '@material-ui/core/styles';
import mainTheme from '../../../theme/mainTheme';
import Button from '@material-ui/core/Button';
import LinearProgress from '@material-ui/core/LinearProgress';

import styles from '../../../utils/pageLayout';

class SOBillListPage extends React.Component {
  constructor(props) {
    super(props);

    if (!getPSITokenId()) {
      router.replace('/auth/login');
    }
  }

  componentDidMount = () => {
  }

  render() {
    const { classes, list, totalPage, page, loading } = this.props;

    return (
      <MuiThemeProvider theme={mainTheme}>
        <CssBaseline />
        <Grid container direction='column' wrap='nowrap' className={classes.r}>
          <Grid item className={classes.header}>
            <AppBar position='static'>
              <Toolbar>
                <IconButton className={classes.menuButton} onClick={() => { router.goBack() }}>
                  <LeftArrowIcon color='inherit' />
                </IconButton>
                <Typography variant="title" color="inherit">
                  销售订单
              </Typography>
              </Toolbar>
            </AppBar>
            <LinearProgress color='secondary' hidden={!loading} />
          </Grid>

          <Grid item className={classes.c}>
            <List>
              {
                list && list.map((it, index) => {
                  return <ListItem key={index} divider button data-id={it.id} onClick={this.onDetail}>
                    <ListItemText>
                      <div>
                        &emsp;单号: {it.ref}
                      </div>
                      <div>
                        交货日: {it.dealDate} / {it.billStatus}
                      </div>
                      <div>
                        &emsp;客户: {it.customerName}
                      </div>
                      <div>
                        销售额: {it.goodsMoney}
                      </div>
                    </ListItemText>
                    <ListItemSecondaryAction>
                      <IconButton>
                        <RightArrowIcon data-id={it.id} onClick={this.onDetail} />
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
                })
              }
              {
                (list && list.length === 0) && <ListItem key={0}>没有销售订单</ListItem>
              }
            </List>
          </Grid>
          {
            list && list.length !== 0 &&
            <Grid item className={classes.f}>
              <Grid container justify='space-around' alignItems='baseline'>
                <Button disabled={page === 1} onClick={this.gotoPrevPage}>上一页</Button>
                {page}/{totalPage}
                <Button disabled={page === totalPage} onClick={this.gotoNextPage}>下一页</Button>
              </Grid>
            </Grid>
          }
        </Grid>
      </MuiThemeProvider >
    );
  }

  gotoPrevPage = () => {
    const { page, dispatch } = this.props;
    if (page === 1) {
      return;
    }

    const prevPage = page - 1;

    dispatch({ type: 'sobillList/querySOBillList', payload: { page: prevPage } });
  }

  gotoNextPage = () => {
    const { page, totalPage, dispatch } = this.props;
    if (page === totalPage) {
      return;
    }

    const nextPage = page + 1;

    dispatch({ type: 'sobillList/querySOBillList', payload: { page: nextPage } });
  }

  onDetail = (event) => {
    const id = event.currentTarget.getAttribute('data-id');
    const { dispatch } = this.props;
    dispatch({ type: 'sobillList/querySOBillInfo', payload: { id } });

    router.push('/sobill/detail');

  }
}

SOBillListPage.propTypes = {
};

function mapStateToProps(state) {
  const { list, totalPage, page } = state.sobillList;

  return {
    list, totalPage, page, loading: state.loading.models.sobillList
  };
}


export default connect(mapStateToProps)(withStyles(styles)(SOBillListPage));

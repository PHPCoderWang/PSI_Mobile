import request from '../../../utils/request';

import { getPSITokenId } from '../../../utils/tokenId';

// service里面的代码通常就是组织参数，然后发起ajax请求

// 使用DVA对service方法的调用就不是直接调用，
// 而是在effects中用 yield的方法间接调用
// 例如：const { data } = yield call(customerService.queryCustomerList, { page });

export function queryCustomerList({ page, qc: { categoryId, code, name, mobile, tel, address, contact, qq } }) {
  var formData = new FormData();
  formData.append('tokenId', getPSITokenId());
  formData.append('page', page);
  formData.append('categoryId', categoryId);
  formData.append('code', code);
  formData.append('name', name);
  formData.append('mobile', mobile);
  formData.append('tel', tel);
  formData.append('address', address);
  formData.append('contact', contact);
  formData.append('qq', qq);

  return request('/web/API/Customer/customerList', {
    credentials: 'include',
    method: "POST",
    body: formData,
  });
}

export function queryCategoryListWithAllCategory() {
  var formData = new FormData();
  formData.append("tokenId", getPSITokenId());

  return request('/web/API/Customer/categoryListWithAllCategory', {
    credentials: 'include',
    method: "POST",
    body: formData,
  });
}

export function queryCategoryList() {
  var formData = new FormData();
  formData.append("tokenId", getPSITokenId());

  return request('/web/API/Customer/categoryList', {
    credentials: 'include',
    method: "POST",
    body: formData,
  });
}

export function queryPriceSystemList() {
  var formData = new FormData();
  formData.append("tokenId", getPSITokenId());

  return request('/web/API/Customer/priceSystemList', {
    credentials: 'include',
    method: "POST",
    body: formData,
  });
}

export function editCustomerCategory({ id, code, name, psId }) {
  var formData = new FormData();
  formData.append('tokenId', getPSITokenId());
  formData.append('id', id);
  formData.append('code', code);
  formData.append('name', name);
  formData.append('psId', psId);

  return request('/web/API/Customer/editCategory', {
    credentials: 'include',
    method: 'POST',
    body: formData,
  });
}

export function queryCustomerCategory({ categoryId }) {
  var formData = new FormData();
  formData.append('tokenId', getPSITokenId());
  formData.append('categoryId', categoryId);

  return request('/web/API/Customer/categoryInfo', {
    credentials: 'include',
    method: 'POST',
    body: formData,
  });
}

export function deleteCustomerCategory({ categoryId }) {
  var formData = new FormData();
  formData.append('tokenId', getPSITokenId());
  formData.append('categoryId', categoryId);

  return request('/web/API/Customer/deleteCategory', {
    credentials: 'include',
    method: 'POST',
    body: formData,
  });
}

export function queryCustomerInfo({ id }) {
  var formData = new FormData();
  formData.append('tokenId', getPSITokenId());
  formData.append('id', id);

  return request('/web/API/Customer/customerInfo', {
    credentials: 'include',
    method: 'POST',
    body: formData,
  });
}

export function queryWarehouseList() {
  var formData = new FormData();
  formData.append('tokenId', getPSITokenId());

  return request('/web/API/Customer/warehouseList', {
    credentials: 'include',
    method: 'POST',
    body: formData,
  });
}

export function editCustomer({ id, categoryId, code, name, address, addressReceipt,
  contact01, mobile01, tel01, qq01, contact02, mobile02, tel02, qq02,
  bankName, bankAccount, tax, fax, initReceivables, initReceivablesDT,
  warehouseId, note }) {
  var formData = new FormData();
  formData.append('tokenId', getPSITokenId());
  formData.append('id', id);
  formData.append('categoryId', categoryId);
  formData.append('code', code);
  formData.append('name', name);
  formData.append('address', address);
  formData.append('addressReceipt', addressReceipt);
  formData.append('contact01', contact01);
  formData.append('mobile01', mobile01);
  formData.append('tel01', tel01);
  formData.append('qq01', qq01);
  formData.append('contact02', contact02);
  formData.append('mobile02', mobile02);
  formData.append('tel02', tel02);
  formData.append('qq02', qq02);
  formData.append('bankName', bankName);
  formData.append('bankAccount', bankAccount);
  formData.append('tax', tax);
  formData.append('fax', fax);
  formData.append('initReceivables', initReceivables);
  formData.append('initReceivablesDT', initReceivablesDT);
  formData.append('warehouseId', warehouseId);
  formData.append('note', note);

  return request('/web/API/Customer/editCustomer', {
    credentials: 'include',
    method: 'POST',
    body: formData,
  });
}

export function deleteCustomer({ id }) {
  var formData = new FormData();
  formData.append('tokenId', getPSITokenId());
  formData.append('id', id);

  return request('/web/API/Customer/deleteCustomer', {
    credentials: 'include',
    method: 'POST',
    body: formData,
  });
}

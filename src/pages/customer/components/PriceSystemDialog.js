import React from 'react';
import { connect } from 'dva';

import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Slide from '@material-ui/core/Slide';

const styles = {
  appBar: {
    position: 'static',
  },
  flex: {
    flex: 1,
  },
};

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class PriceSystemDialog extends React.Component {
  onClick = (event) => {
    const id = event.currentTarget.getAttribute('data-id');
    const text = event.currentTarget.getAttribute('data-name');

    const { onPriceSystemSelect, onClose } = this.props;
    onClose();
    onPriceSystemSelect({ id: id, text: text });
  }

  componentDidMount = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'customerCategoryList/queryPriceSystemList',
      payload: {}
    });
  }

  render() {
    const { classes, open, onClose, list, ...other } = this.props;

    return (
      <Dialog onClose={onClose} open={open} {...other} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <Typography variant="title" color="inherit" className={classes.flex}>
              选择价格体系
            </Typography>
          </Toolbar>
        </AppBar>
        <List>
          {
            list && list.map((it, index) => {
              return <ListItem key={index} button divider onClick={this.onClick}
                data-id={it.id} data-name={it.name}>
                <ListItemText>{it.name}</ListItemText>
              </ListItem>
            })
          }
        </List>
      </Dialog>
    );
  }
}

PriceSystemDialog.propTypes = {
};

function mapStateToProps(state) {
  const { psList } = state.customerCategoryList;
  return {
    list: psList
  };
}

export default connect(mapStateToProps)(withStyles(styles)(PriceSystemDialog));

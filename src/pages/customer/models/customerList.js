import * as customerService from '../services/customerService';

// 在UmiJS下，models目录下的自动注册为DVA Model
// 在PSI中，把model的文件名和其namespace一一对应
// 例如：customerList.js 的namespace就是customerList

export default {
  namespace: 'customerList',
  state: {
    // 客户List
    list: [],

    // 当前选中或编辑的客户资料
    entity: {
      id: '',
    },

    // 仓库List
    warehouseList: [],

    // 记录总数 - 客户list
    totalCount: 0,

    // 总页数 - 客户list
    totalPage: 0,

    // 当前页码 - 客户list
    page: 1,

    // 查询条件中选择的客户分类id
    //categoryId: '-1',
    // 查询条件中选择的客户分类名称
    //categoryName: '[所有分类]',

    // 查询条件 qc == QueryCondition
    qc: {
      categoryId: '-1',
      categoryName: '[所有分类]',
      code: '',
      name: '',
      mobile: '',
      tel: '',
      address: '',
      contact: '',
      qq: ''
    }
  },

  // reducers中的方法是修改state的地方
  reducers: {
    /**
     * 同步state
     */
    saveCustomerList(state, { payload: { data: list, totalCount, totalPage, page } }) {
      return { ...state, list, totalCount, totalPage, page };
    },

    saveEntity(state, { payload: { entity } }) {
      return { ...state, entity };
    },

    saveQueryCondition(state, { payload: { qc } }) {
      return { ...state, qc };
    },

    saveWarehouseList(state, { payload: { warehouseList } }) {
      return { ...state, warehouseList };
    }
  },

  // effects中通常是处理异步调用的地方(action)
  // 常见的业务场景：调用service里面的方法发起ajax获取数据，然后调用reducers里面的方法同步state
  // effects里面的方法都是generator
  effects: {
    // 函数前面加了* 表示该函数是个generator，其内部有yield做异步调用
    *queryCustomerList({ payload: { page, qc } },
      { call, put }) {
      // 异步调用customerService的方法queryCustomerList
      const { data: result } = yield call(customerService.queryCustomerList, { page, qc });

      // 上一步异步调用结束后，调用reducers里面的saveCustomerList方法同步state
      yield put({
        type: 'saveCustomerList',
        payload: {
          data: result.dataList,
          totalCount: result.totalCount,
          totalPage: result.totalPage,
          page
        },
      });
    },

    *queryCustomerInfo({ payload: { id } },
      { call, put }) {
      const { data: entity } = yield call(customerService.queryCustomerInfo, { id });

      yield put({
        type: 'saveEntity',
        payload: {
          entity
        },
      });
    },

    *queryWarehouseList({ payload },
      { call, put }) {
      const { data: warehouseList } = yield call(customerService.queryWarehouseList);
      yield put({
        type: 'saveWarehouseList',
        payload: {
          warehouseList
        },
      });
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      // 监听url变化
      return history.listen(({ pathname, query }) => {
        if (pathname === '/customer') {
          // 通常是首次进入客户资料页面
          // 用dispatch调用effects的queryCustomerList
          // payload是调用的参数，这里是url后面的参数对象
          // 例如: /customer?page=3&code=102 的时候
          // query = {page:3, code:102}
          //dispatch({ type: 'queryCustomerList', payload: query });
        }
      });
    },
  },
};
